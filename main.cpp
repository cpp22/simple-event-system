#include <iostream>
#include <memory>
#include <utility>
#include <functional>

#include "iBaseMassage.hpp"
#include "messageHandler.hpp"


class ChatMessage : public IBaseMessage {
public:
	ChatMessage(std::string sender, std::string msg) : sender(std::move(sender)), msg(std::move(msg)) {}

	[[nodiscard]] EMessageType getType() const override {
		return EMessageType::Move;
	}

	std::string sender;
	std::string msg;
};


class App {
	static std::shared_ptr<App> sApp;
public:
	static std::shared_ptr<App> GetApp() {
		if (!sApp) {
			sApp = std::make_shared<App>();
		}
		return sApp;
	}

	MessageHandler<> msgHandler;
};

std::shared_ptr<App>App::sApp;

int main() {
	std::cout << "Hello, World!" << std::endl;

	std::function<void(const std::shared_ptr<IBaseMessage> &msg)> chatLog = [](
			const std::shared_ptr<IBaseMessage> &msg) -> void {
		const auto chat = std::static_pointer_cast<ChatMessage>(msg);
		std::cout << chat->sender << " says: " << chat->msg << '\n';
	};

	auto handler = App::GetApp();

	auto msgP = std::make_shared<ChatMessage>("test", "Hello");
	handler->msgHandler.registerMsg(msgP->getType(), chatLog);
	handler->msgHandler.post(std::static_pointer_cast<ChatMessage>(msgP));

	bool isRunning = true;
	int count = 0;
	while (isRunning && count <= 10) {
		handler->msgHandler.update();
		count++;
	}

	return 0;
}
