//
// Created by p6 on 7/20/21.
//

#ifndef _WEEK10_MESSAGEHANDLER_
#define _WEEK10_MESSAGEHANDLER_

#include <deque>
#include <map>
#include "iBaseMassage.hpp"

template<typename returnValue = void, typename T = std::shared_ptr<IBaseMessage> >
class MessageHandler {

protected:
	std::deque<std::pair<EMessageType, T>> queue;
	std::multimap<EMessageType, std::function<returnValue(const T &msg)> > callBackMMap;//priority queue test later

	void dispatch(EMessageType id, T msg) {
		auto r = callBackMMap.equal_range(id);
		for (auto it = r.first; it != r.second; ++it) {
			it->second(msg);
		}
	}//actually deliver the msg

public:
	void post(T msg) {
		queue.push_back(std::pair<EMessageType, T>(msg->getType(), msg));
	}//into de future

	void send(T msg) {
		dispatch(msg->getType(), msg);
	}//send and deliver immediately

	void registerMsg(EMessageType id, std::function<returnValue(const T &msg)> &callback) {
		callBackMMap.template insert(std::pair<EMessageType, std::function<returnValue(const T &msg)> >(id, callback));
	}

	void update() {
		size_t szQ = queue.size();
		while (szQ) {
			//TODO max amount
			//TODO timestamp, allow posting N ms in the future
			dispatch(queue.front().first, queue.front().second);
			queue.pop_front();
			szQ--;
		}// process msgs
	}
};


#endif //_WEEK10_MESSAGEHANDLER_
