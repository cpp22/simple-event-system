//
// Created by p6 on 7/20/21.
//

#ifndef _WEEK10_IBASEMASSAGE_
#define _WEEK10_IBASEMASSAGE_

enum class EMessageType : uint32_t {
	Chat,
	Move,
	Spawn,
	Quit,
	Dance
};


class IBaseMessage {
public:
	virtual EMessageType getType() const = 0;
};

#endif //_WEEK10_IBASEMASSAGE_
